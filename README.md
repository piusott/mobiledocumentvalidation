Mobile Barcode Validation App

This app is used to verify that a document is genuine and has not been tampered with. This is
done via verifying the signature of a PDF417 or QR barcode on the document.

Currently this app works offline, which means that there is no backend validation of the data. However we do periodically synch the valid document types with a server. Depending on user requirements, we may end up integrating this more fully into ABCorp's production systems to allow online data validation and retrieval of additional information, e.g. photos, additional perso data, etc.

Pius Ott
ABCorp 2017
