/**
 * ABCorp Barcode Validation app - ios startup
 * At this stage this is identical to the android startup
 * Pius Ott - 20170721
 * @flow
 */

'use strict'
import React, { Component } from 'react';
import {
   AppRegistry,
 } from 'react-native';

//import BackgroundTimer from 'react-native-background-timer';

import MainMenu from './source/mainMenu.js';
import {StackNavigator} from 'react-navigation';
import ScanScreen from './source/scanScreen';
import StatsScreen from './source/statsScreen';
import DocScreen from './source/docScreen';
import ServerSynchScreen from './source/serverSynchScreen';
import SetupScreen from './source/setupScreen';
import AboutScreen from './source/aboutScreen';

// --------------------------------------------------------------------------------------------------------------------

/*
// Start a timer that runs continuous after X milliseconds
const intervalId = BackgroundTimer.setInterval(() => {
    // this will be executed every 2000 ms
    // even when app is the the background
    console.log('*** Running interval docDefSynch');

    // loadServerDocDef uses serverURL initialised in loadConfigSettings, so use a promise here
    this.config.loadConfigSettings()
      .then( () => { this.docDefs.loadServerDocDef(); })
      .catch(error => { console.log('*** Error on loading config settings : ' + error); });

}, 60000);  // for testing, run this every minute. Normally we'd run this just once per day
*
// Cancel the timer when you are done with it
BackgroundTimer.clearInterval(intervalId);

*/

// --------------------------------------------------------------------------------------------------------------------

const BarcodeValidation = StackNavigator(
{
  ////// temp, making this the startup screen while I'm designing it, so I don't have to keep pushing buttons...
  //StatsScreen: { screen: StatsScreen },

  Home: { screen: MainMenu },
  ScanScreen: { screen: ScanScreen },
  DocScreen: { screen: DocScreen },
  ServerSynchScreen: { screen: ServerSynchScreen },
  SetupScreen: { screen: SetupScreen },
  AboutScreen: { screen: AboutScreen },
  StatsScreen: { screen: StatsScreen, title: 'Statistics' },
},
{
  //headerMode: 'none',
  headerMode: 'screen',
  //headerMode: 'float',
}
);

// --------------------------------------------------------------------------------------------------------------------

AppRegistry.registerComponent('BarcodeValidation', () => BarcodeValidation);
