/**
 * ABCorp Barcode Validation app
 * Server synchronisation logging class
 * Pius Ott - 20180131
 */

'use strict'
import React, { Component } from 'react';
import {
   AsyncStorage,
} from 'react-native';
import Moment from 'moment';
var _ = require('lodash/core');

// --------------------------------------------------------------------------------------------------------------------

const storageKey = '@BarcodeVal:serverSynchLog';    // Location of our local log file

global.synchLogData = [];

export default class ServerSynchLog extends Component {

  constructor(props)
  {
    super(props);
/*
    /////// Testing, initialise it to something I can display here.
    synchLogData =
    [
      {key: 1513401041808, status: 'Success', tasks: 'Local Update', url:'http://abidtest.abnote.com.au/barcodevaltest/'},
      {key: 1517541241808, status: 'Success', tasks: '', url:'http://abidtest.abnote.com.au/barcodevaltest/'},
      {key: 1517541341808, status: 'Success', tasks: '', url:'http://172.31.64.64/barcodeval/'},
      {key: 1517541401808, status: 'Failed', error: 'No connection to server', tasks: '', url:''},
      {key: 1516441141808, status: 'Failed', error: 'No connection to server', tasks: '', url:''},
      {key: 1517541411808, status: 'Success', tasks: '', url:''},
      {key: 1517541421808, status: 'Success', tasks: '', url:''},
      {key: 1517541441808, status: 'Success', tasks: '', url:''},
    ];
*/
    this.LoadSynchLog();    // load saved log
    // sort by date/time
    synchLogData = _.sortBy(synchLogData, 'key', function(n) { return Math.sin(n); });

    for(let i = 0; i < synchLogData.length; i++)
    {
      console.log('*** ' + i + ' - ' + synchLogData[i].key);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  static UpdateSynchLog(status, error, tasks)
  {
    var now = Moment();
    console.log('*** UpdateSynchLog : status [' + status + '] - error [' + error + '] - tasks [' + tasks + ']');

    const maxListSize = 8;    // number of entries we want to show in our list

    /*
    for (let i = 0; i < synchLogData.length; i++)
    {
      console.log('*** Original ' + i + ' : ' + Moment(synchLogData[i].key).format('D MMMM YYYY - h:mm:ss a'));
    } */

    if (synchLogData.length >= maxListSize)
    {
      console.log('*** Replacing old entry in synchLogData');
      // we assume that the list is sorted, so replace the oldest, i.e. first, entry
      synchLogData[0].key = now;
      synchLogData[0].status = status;
      synchLogData[0].error = error;
      synchLogData[0].tasks = tasks;
      synchLogData[0].url = configSettings.serverURL;

      // sort the list again
      synchLogData = _.sortBy(synchLogData, 'key', function(n) { return Math.sin(n); });
    }
    else
    {
      console.log('*** Adding new entry in synchLogData. Current Size : ' + synchLogData.length);

      var tmpArray = synchLogData.slice();
      tmpArray.push({key: now, status: status, tasks: tasks, error: error, url:configSettings.serverURL});
      synchLogData = _.sortBy(tmpArray, 'key', function(n) { return Math.sin(n); });
    }

    this.SaveSynchLog();

    /*
    for (let i = 0; i < synchLogData.length; i++)
    {
      console.log('*** New ' + i + ' : ' + Moment(synchLogData[i].key).format('D MMMM YYYY - h:mm:ss a'));
    }
    */
  }

  // ------------------------------------------------------------------------------------------------------------------

  static SaveSynchLog()
  {
    try
    {
      console.log("*** Saving synchLogData");
      AsyncStorage.setItem(storageKey, JSON.stringify(synchLogData));
    }
    catch (e)
    {
      console.log("*** Could not save synchLogData to local : " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  LoadSynchLog()
  {
    try
    {
      AsyncStorage.getItem(storageKey, (err, result) =>
      {
        let synchLog = JSON.parse(result)
        console.log("*** loaded synchLog : " + result);
        if (synchLog != null)
        {
          synchLogData = synchLog;
          console.log("*** Loaded synchLogData from local");
        }
        else
        {
          synchLogData = [];  // initialise a blank log
        }
      });
    }
    catch (e)
    {
      console.log("*** Could not load synchLogData from local : " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

}
