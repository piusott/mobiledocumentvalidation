/**
 * ABCorp Barcode Validation app
 * Document Definition Synch class
 * Pius Ott - 20171127
 */

 'use strict'
 import React, { Component } from 'react';
 import {
   Dimensions,
   StyleSheet,
   Text,
   TouchableHighlight,
   Button,
   View,
   Alert,
   AsyncStorage,
 } from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob';
import ServerSynchLog from './serverSynchLog';

// --------------------------------------------------------------------------------------------------------------------

global.docDefinitions = null;

export default class DocDefSynch extends Component {

  constructor(props)
  {
    super(props);

    this.state = {
      //cameraType: Camera.constants.Type.back,
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  /**
  * Load the document definitions. This is done as follows :
  * 1. Attempt to load the local copy.
  * 2. Try and connect to the synch server to receive the latest definitions.
  * 3. If the fetch was successful, update our local copy with the server data and update our loaded definitions
  * This way we synch whenever we can, but also allow working completely offline.
  * The whole process could probably be sped up, e.g. add a function to the server that just tells us the latest date
  * of the docDefs and we only perform a fetch if needed. We could also look at using individual docdef files rather
  * than a big one that contains all.
  */
  loadServerDocDef(useCache)
  {
    //const sampleDocDefName = "VT432";   // used for testing
    if (useCache === true)
    {
      // try and load the docDefinitions from our local storage
      try
      {
        AsyncStorage.getItem('@BarcodeVal:docdef', (err, result) =>
        {
          let docDef = JSON.parse(result)
          //console.log("*** result : " + result);
          //console.log("*** docDef : " + docDef);
          if (docDef != null)
          {
            docDefinitions = docDef.definition;
            console.log("*** Loaded docDefinitions from local");

            // testing, make sure we've loaded the definitions
            //let docDef2 = this.getDocDef(sampleDocDefName);
            //console.log("*** Store SubTitle : " + docDef2.SubTitle);
          }
        });
      }
      catch (e)
      {
        console.log("*** Could not load docDefinitions from local : " + e);
      }
    }

    console.log('*** Now checking docDefinitions from server : ' + configSettings.serverURL + 'docDefinitions.json');
    // now attempt to fetch the current docDefinitions from the server
    fetch(configSettings.serverURL + 'docDefinitions.json', {
      headers: {
        'Cache-Control': 'no-cache'
      }})
      .then((response) => response.json())
      .then((responseJson) =>
      {
        //console.log("*** Fetched JSON data successfully - updating local store");

        try
        {
          //console.log("*** Fetched JSON : " + responseJson);
          //let docDef3 = this.getDocDef(sampleDocDefName);
          //console.log("*** Fetch SubTitle : " + docDef3.SubTitle);
          // Save the fetched definitions locally, overwriting any previous definitions

          AsyncStorage.setItem('@BarcodeVal:docdef', JSON.stringify(responseJson));

          var updateDesc = 'No update required';

          // I'm sure there's a better way to compare these two objects than converting them to a string, but so far
          // the comparison always fails if I try to compare them without conversion, even if the object data matches.
          var responseJsonString = JSON.stringify(responseJson.definition);
          var docDefinitionsString = JSON.stringify(docDefinitions);
          if (docDefinitionsString !== responseJsonString)
          {
            updateDesc = 'Update local cache';
            // update our dynamic definitions with the new server ones
            docDefinitions = responseJson.definition;
          }

          this.synchLogos();

          ServerSynchLog.UpdateSynchLog('Success', '', updateDesc);
        }
        catch (e)
        {
          console.log("*** Could not save docDefinitions to local : " + e);
          ServerSynchLog.UpdateSynchLog('Failed', 'Error saving to cache.', '');
        }

      })
      .catch((error) => {
        console.log("*** Could not fetch latest docDefinitions - " + error);
        if ((docDefinitions === null) || (docDefinitions === undefined))
        {
          this.createDemoDocDefs();
          ServerSynchLog.UpdateSynchLog('Failed', 'Using default docs', '');
        }
        else
        {
          ServerSynchLog.UpdateSynchLog('Failed', 'Error contacting server', '');
          console.log("*** Will use local copy of docDefinition");
        }
      });
  }

  // ------------------------------------------------------------------------------------------------------------------

  // If we haven't been able to connect to the synch server and have no locally stored docDefs, load a pre-defined
  // demo set
  createDemoDocDefs()
  {
    try
    {
      console.log("*** No docDefinitions available. Creating a demo set.");
      docDefinitions = require('../docTypes/docDefinitions.json').definition;

      AsyncStorage.setItem('@BarcodeVal:docdef', JSON.stringify(docDefinitions));

    }
    catch (e)
    {
      console.log("*** Exception in createDemoDocDefs - " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  // once we've synched the docDef JSON, go through all referenced logos and make sure they're still in synch
  synchLogos()
  {
    function getImageBase64(imgFileName, mimetype_attachment, serverURL)
    {
      var fullServerURL = serverURL + 'images/' + imgFileName;
      //console.log("*** Using server URL : " + fullServerURL);

      return new Promise((RESOLVE, REJECT) =>
      {
        // Fetch attachment
        RNFetchBlob.fetch('GET', fullServerURL).then((response) =>
        {
          let base64Str = response.data;
          //console.log("*** received : " + base64Str);
          var imageBase64 = 'data:' + mimetype_attachment + ';base64,' + base64Str;
          //console.log("*** base64Str is type : " + typeof(base64Str));

          RESOLVE(imageBase64)  // return base64 image
        })
      }).catch((error) => {
         // error handling
        console.log("*** Error fetching image : ", error)
      });
    }

    for (let docDef of docDefinitions)
    {
      getImageBase64(docDef.Logo, 'image/jpeg', configSettings.serverURL).then((base64Response) =>
      {
        if (base64Response.lastIndexOf('404 Not Found') < 0)
        {
          console.log("*** We have an image. Store [" + docDef.Logo + "] in AsyncStorage");
          try
          {
            AsyncStorage.setItem('@BarcodeVal:' + docDef.Logo, base64Response);
          }
          catch (e)
          {
            console.error("*** Could not store image : @BarcodeVal:" + docDef.Logo + " - Error : " + e);
          }
        }
      });
    }

  }

  // ------------------------------------------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------------------------------------------

  static getDocDef(shortName)
  {
    function FindShortName(element)
    {
      return element.ShortName === shortName;
    }

    return docDefinitions.find(FindShortName);
  }

  // ------------------------------------------------------------------------------------------------------------------
}
