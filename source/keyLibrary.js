/*
Temp object that holds all our valid public keys.
NOTE : This has been replaced by the docDefinitions which also hold the PubKey for all document docTypes.
       I'm just leaving this class in for reference at the moment, but eventually it will need to be removed.
*/

'use strict'
import React, { Component } from 'react';

export default class keyLibrary extends Component
{

  // ------------------------------------------------------------------------------------------------------------------

  static loadKey(docType)
  {
    // Note that this function would normally pick the correct key from the library of available public keys. I'm just
    // hardcoding the keys we used for the PoC here.

    // We only need to know the public keys since all we're doing is validating a signature
    var keySet = {
      'VT431':
      {
        'public':
        '-----BEGIN PUBLIC KEY-----\n' +
        'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEyTQTzKqZWJ++xqTMlYYNiRMi8UaP\n' +
        'rtpX3+/VT4edFe+svijeDlMpTxE8UE8LCzg8sqjNNY02cnoneDwabtW2qg==\n' +
        '-----END PUBLIC KEY-----'
      },
      'B1234':
      {
        'public':
        '-----BEGIN PUBLIC KEY-----\n' +
        'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEQzeMm/tvb/BV7swWaTO333OOgNeB\n' +
        'N56QZVfiCVkwmWpZSLSAP/S4aHwHn/TeTC4spz81Ack9D4xMc0RYQJ4VNw==\n' +
        '-----END PUBLIC KEY-----'
      },
      'M0001':
      {
        'public':
        '-----BEGIN PUBLIC KEY-----\n' +
        'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAETViJ5C5v8NsEMNGG5fSn7d4F7XVU\n' +
        'ciV1LTPRdAzfHMb8OoXVwXJjNHtB7k2SRsM8RbozstFntDvGan+n1GxTQw==\n' +
        '-----END PUBLIC KEY-----'
      },
      'U9876':
      {
        'public':
        '-----BEGIN PUBLIC KEY-----\n' +
        'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAESWIPEnwS/oe7yhVhGfeYoAT5f5cc\n' +
        'RmUneglBuWCDP5jA/d36B/w1XUva8/IJnIQNA1sWE5IzJsxoxwm1u16Xew==\n' +
        '-----END PUBLIC KEY-----'
      }
    }

    keySet.public = new Buffer(keySet[docType].public, 'ascii')

    return keySet;
  }

  // ------------------------------------------------------------------------------------------------------------------

}
