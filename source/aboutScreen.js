/**
 * ABCorp Barcode Validation app
 * Pius Ott - 20170721
 * @flow
 */

'use strict'
import React, { Component } from 'react';
import {
  Button,
  Text,
  View,
  ScrollView,
  Linking,
  Image,
  ImageBackground,
  TouchableHighlight,
  SafeAreaView,
} from 'react-native';

var DeviceInfo = require('react-native-device-info');

class AboutScreen extends Component {

  constructor(props)
  {
    super(props);

    // Note : for IOS, update the info.plist file (or set it in XCode) for later build versions. I haven't
    //        found out where to do this for Android yet...
    console.log('*** Build : ' + DeviceInfo.getReadableVersion());
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    const { navigate, goBack } = this.props.navigation;

    return (
      <SafeAreaView style={styles.safeArea}>
        <ImageBackground
          style={{flex:1, height: null, width: null}}
          source={require('./images/AboutBackdrop.png')}
          resizeMode="contain" >

          <View style={{height: '42%'}}>
          {/* Push the text towards the bottom so we don't write over the background image's detailed area */}
          </View>

          <ScrollView style={{margin:'1%'}}>

            <Text style={[styles.dataTextTitle, {color: colors.fgTextLight}]}>
              Mobile Document Verification
            </Text>
            <Text style={styles.infoText}>
              This app allows the verification of ABCorp or ABCorp customer issued documents. It verifies that the
              document is genuine and has not been altered post issuance.
            </Text>
            <Text style={styles.infoText}>
              ABCorp has been assisting governments and world-class companies for 220+ years. When it comes to secure
              access, credentials and payments – no other company has more experience. Over the years we have done many
              things, but resolving complex problems is what we do best. If you question how to adapt in a rapidly
              changing world, it would be fair to say we have some experience in that area – more than two centuries of it.
            </Text>
            <Text style={styles.infoText}>
              Build : {DeviceInfo.getReadableVersion()}
            </Text>
            <Text style={styles.infoTextCenter}>
              Visit
              <Text style={styles.infoText, {fontWeight: 'bold'}}
              onPress={() => Linking.openURL('http://abcorp.com')}> abcorp.com</Text>
            </Text>

          </ScrollView>

          <TouchableHighlight
            style={styles.submitSmall}
            onPress={() => goBack(null)}
            underlayColor='#fff'>
            <Text style={styles.submitText}>Home</Text>
          </TouchableHighlight>

        </ImageBackground>
      </SafeAreaView>
    );
  }

  // --------------------------------------------------------------------------------------------------------------------

}

AboutScreen.navigationOptions = {
  header: null
};

export default AboutScreen;
