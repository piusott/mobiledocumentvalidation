/**
 * ABCorp Barcode Validation app
 * Pius Ott - 20170721
 * @flow
 */

 'use strict'
 import React, { Component } from 'react';
 import {
   Button,
   Text,
   View,
   ScrollView,
   SafeAreaView,
   Image,
   ImageBackground,
   AsyncStorage,
   TouchableHighlight,
 } from 'react-native';

import CameraScanner from './cameraScanner';
import Moment from 'moment';
import Crypto from 'crypto';
import DocDefSynch from './docDefSynch';
import ScanLog from './scanLog';
import BusyIndicator from './busyIndicator';

const styles = require('./style.js').style;
const colors = require('./style.js').color;

// --------------------------------------------------------------------------------------------------------------------

class ScanScreen extends Component {

  constructor(props)
  {

    super(props);
    this.state =
    {
      isValid: false,
      isABCorpBarcode: true,
      hasBarcode: false,
      logoImg: null,
      isValidating: false,
    }

    this.barcodeClearedHandler = this.resetBarcodeData.bind(this);

    this.resetBarcodeData();
  }

  // ------------------------------------------------------------------------------------------------------------------

  barcodeReceived(barcodeData)
  {
    console.log('*** barcodeReceived handler called with param : ' + barcodeData);
    this.setState({
      isValidating: true,
    });

    this.barcodeData = barcodeData;

    // Run this in a slight delay so we can let the screen refresh and show the busy indicator while deciphering the data
    setTimeout(() => { this.decipherBarcode() }, 5);
  }

  // ------------------------------------------------------------------------------------------------------------------

  resetBarcodeData()
  {
    console.log('Resetting barcode and starting another capture');

    // checking the refs is required to silence the 'setState can only update mounted or mounting...'' warning
    if (this.refs.dummy)
    {
      this.setState({
        isValid: false,
        hasBarcode: false,
        isABCorpBarcode : true,
        logoImg: null,
        isValidating: false,
      });
    }

    this.currentDocDef = null;

    this.barcodeDetails = {
      barcodeData: '-',
      docType: '',
      holderName: '',
      holderId: '',
      dob: '',
      issueDate: '',
      timeStamp: '',
      freeFormText: '',
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  // Verify the signature to find out if this is a valid barcode or a forged one.
  verifyData()
  {
    var resultVer = false;
    var recognisedBarcode = false;

    var indexOfSignature = this.barcodeData.lastIndexOf('|');
    var signatureString = this.barcodeData.substring(indexOfSignature + 1, this.barcodeData.length);
    var messageString = this.barcodeData.substring(0, indexOfSignature + 1);

    //console.log("*** signature : " + signatureString);
    //console.log("*** message : " + messageString);

    var splitData = this.barcodeData.split('|');
    if (splitData.length >= 8)
    {
      // Now try to verify the signature created in .NET
      // TODO : Do we need to create the crypto object every time or can we speed up the process by creating this globally?
      var myVer = Crypto.createVerify('sha256')
      myVer.update(messageString)

      this.currentDocDef = DocDefSynch.getDocDef(splitData[0]);
      if (this.currentDocDef != null)
      {
        if (this.currentDocDef.Logo != null)
          this.updateLogo(this.currentDocDef.Logo);   // load the logo from AsyncStorage
        else
          console.log('*** No Logo available for [' + splitData[0] + ']');

        //console.log("*** SubTitle : " + this.currentDocDef.SubTitle);
        //console.log("*** Using JSON key for " + splitData[0]);
        var keyDetails = new Buffer(this.currentDocDef.PublicKey, 'ascii')
        resultVer = myVer.verify(keyDetails, signatureString, 'base64')

        ScanLog.UpdateScanLog(splitData[0], resultVer, this.barcodeData);
        recognisedBarcode = true;
      }
      else
        console.log('*** No document definition available for [' + splitData[0] + ']');
    }
    return [resultVer, recognisedBarcode];
  }

  // ------------------------------------------------------------------------------------------------------------------

  updateLogo(fileName)
  {
    try
    {
      //AsyncStorage.getItem('@BarcodeVal:' + fileName, (err, result) =>
      AsyncStorage.getItem('@BarcodeVal:' + fileName, (err, result) =>
      {
        let img64 = result;
        if (img64 !== null)
        {
          //console.log("*** Loaded image [@BarcodeVal:" + fileName + "] from local");

          this.setState({
            logoImg: img64
          });
        }
      });
    }
    catch (e)
    {
      console.log("*** Could not load new @BarcodeVal:" + fileName + " from local : " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  decipherBarcode()
  {
    var isABCorpBarcode = false;
    var isValid = false;

    [isValid, isABCorpBarcode] = this.verifyData();
    console.log('*** isValid, isABCorpBarcode : ' + isValid + ' - ' + isABCorpBarcode);
    if (isValid)
    {
      console.log('*** Signature Is Valid');
    }
    else
    {
      console.log('*** Signature Is Not Valid');
    }

    // the barcode data is expected to be pipe delimited
    var splitData = this.barcodeData.split('|');
    console.log('splitdata length = ' + splitData.length);
    console.log('Barcode Data : ' + this.barcodeData);

    if (splitData.length >= 9)
    {
      this.barcodeDetails.docType = splitData[0];    // ToDo : translate the doctype code to a readable string
      this.barcodeDetails.holderName = splitData[4];
      this.barcodeDetails.holderId = splitData[6];
      this.barcodeDetails.freeFormText = splitData[7];
      this.barcodeDetails.issueDate = splitData[3];
      this.barcodeDetails.dob = splitData[5];
      this.barcodeDetails.timeStamp = splitData[2];
    }
    else
    {
      this.barcodeDetails.docType = '';
      this.barcodeDetails.holderName = '';
      this.barcodeDetails.holderId = '';
      this.barcodeDetails.freeFormText = '';
      this.barcodeDetails.dob = '';
      this.barcodeDetails.issueDate = '';
      this.barcodeDetails.isValid = false;
      this.barcodeDetails.timeStamp = '';
      console.log("Invalid Barcode");
    }

    console.log(this.docType);
    //AlertIOS.alert("Detected Barcode", "Type: " + e.type + "\nData : " + e.data);
    console.log("Barcode: " + this.barcodeData);

    this.setState({
      isValid: isValid,
      isABCorpBarcode: isABCorpBarcode,
      hasBarcode: isABCorpBarcode,
      isValidating: false,
    });
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const { navigate, goBack } = this.props.navigation;

    var logo;
    if (this.state.logoImg !== null)
    {
      logo = <Image style={styles.docLogo} source={{uri: this.state.logoImg}} />
    }

    // define the text/button/indicator that shows whether the barcode is valid or not
    var validLabel;
    if (this.state.isValid)
    {
      validLabel =
      <View style = {styles.validStateBox} ref="dummy">
        <Text
          style={styles.validStateLabelPass}
          onPress={() => this.barcodeClearedHandler()} >
          Legitimate Issue
        </Text>
        <Image style={styles.ticklogo} source={require('./images/tick.png')} />
      </View>
    }
    else if ((this.barcodeDetails.docType) && (this.barcodeDetails.docType.length > 0))
    {
      validLabel =
      <View style = {styles.validStateBox} ref="dummy">
        <Text
          style={styles.validStateLabelFail}
          onPress={() => this.barcodeClearedHandler()}>
          Fraudulent Issue
        </Text>
        <Image style={styles.ticklogo} source={require('./images/cross.png')} />
      </View>
    }

    // ----------
    if (this.state.hasBarcode)
    {
      return (
        <SafeAreaView style={styles.safeArea}>
          <Text style={[styles.dataTextTitle, {color: colors.fgTextLight}]}>
            Secure Document Details
          </Text>

          <View style={styles.containerBackdrop}>
            <ShowDocumentDetails
              barcodeDetails={this.barcodeDetails}
              currentDocDef={this.currentDocDef}
              barcodeData={this.barcodeData}
              logo={logo}
              isValid={this.state.isValid}/>
            {validLabel}
          </View>

          <View style={{backgroundColor: colors.headerColor}}>
            <TouchableHighlight
              style={styles.submitSmall}
              onPress={() => goBack(null)}
              underlayColor='#fff'>
              <Text style={styles.submitText}>Home</Text>
            </TouchableHighlight>
          </View>
        </SafeAreaView>
      );
    }
    else
    {
      return (
        <SafeAreaView style={styles.safeArea}>

          <BusyIndicator validating={this.state.isValidating} displayText='Validating Barcode' />

          <Text style={[styles.dataTextTitle, {color: colors.fgTextLight}]}>
            Scan Document Barcode
          </Text>

          <View style={styles.containerBackdrop}>

            <View style={styles.containerCamera}>
              <CameraScanner
                barcodeReceivedHandler = {this.barcodeReceived.bind(this)}
                isABCorpBarcode = {this.state.isABCorpBarcode}/>
            </View>

          </View>

          <View style={{backgroundColor: colors.headerColor}}>
            <TouchableHighlight
              style={styles.submitSmall}
              onPress={() => goBack(null)}
              underlayColor='#fff'>
              <Text style={styles.submitText}>Home</Text>
            </TouchableHighlight>
          </View>
        </SafeAreaView>
      );
    }
  }
}

ScanScreen.navigationOptions = {
  header: null
};

// --------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------

class ShowDocumentDetails extends Component
{

  varTextTitle = [];    // variable field titles
  varTextData = [];     // variable field data

  constructor(props)
  {
    super(props);

    this.formatVarFields();
  }

  // ------------------------------------------------------------------------------------------------------------------

  formatVarFields()
  {
    // can we not read the font size from the style sheet? Or at least make it a constant used everywhere?
    const defaultFontSize = 14;  // default

    var splitData = this.props.barcodeData.split('|');
    for (let fieldDef of this.props.currentDocDef.OfflineFields)
    {
      if (fieldDef.DisplayPosition < splitData.length)
      {
        let varData = splitData[fieldDef.BarPosition];

        //console.log('*** Processing position ' + fieldDef.BarPosition + ' : [' + varData + '] as ' + fieldDef.Type);
        switch (fieldDef.Type)
        {
          case 'TimeStamp':
            // There may be a bug in moment.js. Unless I create a moment with a valid date string, the subsequent
            // moment, initialised with a unix epoch, often fails. So for the moment I'm just patching this by
            // creating a dummy moment.
            let tmpDay = Moment('19650914');  // what a date!

            try
            {
              let seconds = varData * 60000;   // the time stamp is in minutes, we need milliseconds
              varData = Moment.utc(seconds).format("DD MMM YYYY - HH:mm");
              //varData = Moment.unix(seconds).format("DD MMM YYYY hh:mm:ss a");
              //console.log("*** TimeStamp : " + varData + " - Seconds : " + seconds);
            }
            catch (e)
            {
              console.log('*** Error converting timestamp [' + varData + '] - ' + e);
              varData = 'Invalid TimeStamp';
            }
            break;

          case 'Date':
            if ((varData) && (varData.length > 0))
            {
              try
              {
                // Although I'd prefer Moment to auto-detect the date to allow me the flexibility of passing multiple
                // date (and time) formats in the barcode, auto detect generates a warning if the string cannot be parsed
                // into a datetime object, I haven't found a way to turn that warning off, so I'm just limiting the
                // format for the time being.
                var d = Moment(varData, 'YYYYMMDD');
                //var d = Moment(varData);
                if((d == null) || (!d.isValid()))
                {
                  console.log('*** Invalid date string, cannot convert to date : [' + varData + ']');
                  varData = 'Invalid Date';
                }
                else
                {
                  if ((fieldDef.Format) && (fieldDef.Format.length > 0))
                  {
                    varData = d.format(fieldDef.Format);
                  }
                  else
                  {
                    varData = d.format('DD MMM YYYY');  // use default formatting
                  }
                }
              }
              catch (e)
              {
                console.log('*** Error converting date [' + varData + '] - ' + e);
                varData = 'Invalid Date!';
              }
            }
            else
              varData = '-';

            break;

            case 'Text':
              if ((fieldDef.Format) && (fieldDef.Format.length > 0))
              {
                //console.log("*** Formatting text : " + varData + " with format : " + fieldDef.Format);
                // for the time being, I'm only supporting the '{0}' replacement holder
                varData = fieldDef.Format.replace(new RegExp('\\{0\\}', 'gm'), varData);
              }
              else
              {
                // Nothing to do, just print as is
              }
              break;

            default:
              // nothing to do, just print it as unformatted text.
              break;
        }

        // place holder to make sure empty lines don't fuck up our label/textdata symetry. I'm sure there's a better
        // way to do this!
        if ((varData) && (varData.length == 0))
          varData = '-';

        let isDefaultColor = true;
        //let fontSize = styles.dataText.fontSize;
        let fontSize = defaultFontSize;

        if ((fieldDef.FontColor) && (fieldDef.FontColor.length > 0))
        {
          if ((fieldDef.FontColor[0] == '#') && (fieldDef.FontColor.length == 7))
            isDefaultColor = false;
        }

        if ((fieldDef.FontSize) && (fieldDef.FontSize.length > 0))
        {
          switch (fieldDef.FontSize)
          {
            case "Small":
              fontSize = 10;
              break;
            case "Large":
              fontSize = 18;
              break;
            default:
              // nothing to do, retain standard font size
              break;
          }
        }

        // can we not read the font size from the style sheet? Or at least make it a constant used everywhere?
        if ((isDefaultColor == true) && (fontSize == defaultFontSize))  // default
        {
          this.varTextData.push(<Text key={fieldDef.DisplayPosition} style={styles.dataText}>{varData}</Text>);
          this.varTextTitle.push(<Text key={fieldDef.DisplayPosition} style={styles.label}>{fieldDef.Title}</Text>);
        }
        else if ((isDefaultColor == false) && (fontSize == defaultFontSize))  // changed colour
        {
          this.varTextData.push(<Text key={fieldDef.DisplayPosition} style={[styles.dataText, {color : fieldDef.FontColor}]}>{varData}</Text>);
          this.varTextTitle.push(<Text key={fieldDef.DisplayPosition} style={styles.label}>{fieldDef.Title}</Text>);
        }
        else if ((isDefaultColor == true) && (fontSize != defaultFontSize))  // changed font size
        {
          this.varTextData.push(<Text key={fieldDef.DisplayPosition} style={[styles.dataText, {fontSize : fontSize}]}>{varData}</Text>);
          this.varTextTitle.push(<Text key={fieldDef.DisplayPosition} style={[styles.label, {fontSize : fontSize}]}>{fieldDef.Title}</Text>);
        }
        else if ((isDefaultColor == false) && (fontSize != defaultFontSize))  // changed colour and font size
        {
          this.varTextData.push(<Text key={fieldDef.DisplayPosition} style={[styles.dataText, {fontSize : fontSize, color : fieldDef.FontColor}]}>{varData}</Text>);
          this.varTextTitle.push(<Text key={fieldDef.DisplayPosition} style={[styles.label, {fontSize : fontSize}]}>{fieldDef.Title}</Text>);
        }
        //console.log("*** Using FontColor : " + fieldDef.FontColor + " - font size : " + fontSize);
      }
    }
/*
    // map
    this.props.currentDocDef.OfflineFields.map((varFieldDef) =>
    {
      this.varTextData.push(<Text key={fieldDef.DisplayPosition} style={styles.dataText}>{fieldDef.Title}</Text>);
    });*/
  }

  // ------------------------------------------------------------------------------------------------------------------

  // Legacy function, used for VT431
  splitVTFreeform = (freeformText) => {
    //1:12877;2:Toyota LX 2014;3:SN6345-2345;4:999 Any Lane,Any City,AS,99999
    var returnData = {
      odometer: '123456789',
      car: '',
      vehicleId: '',
      address1: '',
      address2: '',
    };

    var splitData = freeformText.split(';');
    if (splitData.length >= 4) {
      var splitOdo = splitData[0].split(':');
      if ((splitOdo.length == 2) && (splitOdo[0] == '1'))
        returnData.odometer = splitOdo[1];

      var splitCar = splitData[1].split(':');
      if ((splitCar.length == 2) && (splitCar[0] == '2'))
        returnData.car = splitCar[1];

      var splitSn = splitData[2].split(':');
      if ((splitSn.length == 2) && (splitSn[0] == '3'))
        returnData.vehicleId = splitSn[1];

      var splitAddr = splitData[3].split(':');
      if ((splitAddr.length == 2) && (splitAddr[0] == '4')) {
        var addressData = splitAddr[1].split(',');
        if (addressData.length >= 4) {
          returnData.address1 = addressData[0];
          // I'm sure there's a better way to do this...
          returnData.address2 = addressData[1] + ', ' + addressData[2] + ', ' + addressData[3];
        }
      }
    }
    return (returnData);
  }

  render()
  {
    // ToDo : Come up with a better and prettier way to signify pass or fail.
    var bgColor = colors.bgPass;

    if (this.props.isValid === false)
    {
      bgColor = colors.bgFail;
    }

    switch (this.props.barcodeDetails.docType)
    {
      case 'VT431':
        var formattedIssue = '';  // legacy, used for VT431
        if ((this.props.barcodeDetails.issueDate) && (this.props.barcodeDetails.issueDate.length > 0))
        {
          console.log("*** this.props.barcodeDetails.issueDate : " + this.props.barcodeDetails.issueDate);
          var formattedIssue = Moment(this.props.barcodeDetails.issueDate).format('DD MMM YYYY');
        }
        var freeformDetails = this.splitVTFreeform(this.props.barcodeDetails.freeFormText);


        return(
          <View style={[styles.containerLicenceDetail, {backgroundColor:bgColor}]}>
            {this.props.logo}
            <Text style={styles.dataTextTitle}>{this.props.currentDocDef.LongName}</Text>

            <View style={{flexDirection: 'row', flex: 2}}>
              <View style={{flex: 1}}>
                <Text style={styles.label}>Credential Number</Text>
                <Text style={styles.label}>Document Number</Text>
                <Text style={styles.label}>Issue Date</Text>
                <Text style={styles.label}>Odometer</Text>
                <Text style={styles.label}>Vehicle Type</Text>
                <Text style={styles.label}>Issued To</Text>
                <Text style={styles.label}>Address</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.dataText}>{this.props.barcodeDetails.holderId}</Text>
                <Text style={styles.dataText}>{freeformDetails.vehicleId}</Text>
                <Text style={styles.dataText}>{formattedIssue}</Text>
                <Text style={styles.dataText}>{freeformDetails.odometer} Miles</Text>
                <Text style={styles.dataText}>{freeformDetails.car}</Text>
                <Text style={styles.dataText}>{this.props.barcodeDetails.holderName}</Text>
                <Text style={styles.dataText}>{freeformDetails.address1}</Text>
                <Text style={styles.dataText}>{freeformDetails.address2}</Text>
              </View>
            </View>
          </View>
        );
      default:  // all other, newer, barcode types
      return(
        <View style={[styles.containerLicenceDetail, {backgroundColor:bgColor}]}>
          {this.props.logo}
          <Text style={styles.dataTextTitle}>{this.props.currentDocDef.LongName}</Text>
          <Text style={styles.dataTextSubTitle}>{this.props.currentDocDef.SubTitle}</Text>

          <ScrollView style={{margin:'1%', backgroundColor:colors.white, paddingTop:8}}>
          <View style={{flexDirection: 'row', flex: 2}}>
            <View style={{flex: 1}}>
              {this.varTextTitle.map((value, index) => { return value})}
            </View>
            <View style={{flex: 1}}>
              {this.varTextData.map((value, index) => { return value})}
            </View>
          </View>
          </ScrollView>
        </View>
      );
    }
  }
}

// --------------------------------------------------------------------------------------------------------------------

export default ScanScreen;
