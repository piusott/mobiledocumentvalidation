/**
 * ABCorp Barcode Validation app
 * Pius Ott - 20170721
 * @flow
 */

'use strict'
import React, { Component } from 'react';
import {
  Button,
  Text,
  View,
  Image,
  ImageBackground,
  ListView,
  AsyncStorage,
  SafeAreaView,
  TouchableHighlight,
} from 'react-native';

import Moment from 'moment';

import Row from './docListRow';

class DocScreen extends Component {

  constructor(props)
  {
    super(props);

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 != r2
    });

    let dsTmp = this.formatData(docDefinitions);

    this.state = {
      ds:dsTmp,
      dataSource:ds.cloneWithRows(dsTmp),
    }

    this.loadLogos();
  }

  // ------------------------------------------------------------------------------------------------------------------

  loadLogos()
  {
    for (let docDef of docDefinitions)
    {
      if (docDef.Logo.length > 0)
      {
        AsyncStorage.getItem('@BarcodeVal:' + docDef.Logo, (err, result) =>
        {
          try
          {
            if ((result !== null) && (result !== undefined))
            {
              //console.log("*** Loaded image [@BarcodeVal:" + docDef.Logo + "] in loadLogos");

              var newDs = [];
              newDs = this.state.ds.slice();

              ////////const docs = newDs.filter((doc) => doc.Logo === docDef.Logo));
              ///// There has to be an easier way to find the index of an array! Still learning ReactNative/js...
              for (let i = 0; i < newDs.length; i++)
              {
                if (newDs[i].ShortName === docDef.ShortName)
                {
                  // We need to create a new object and insert it in place of the old object. Just updating the object
                  // data does not work, as I found out after a long battle...
                  // Updating the object and setting the state does call a new render, but the renderRow function of the
                  // ListView is not called unless the whole object has been replaced!
                  newDs[i] = {
                    ...this.state.ds[i],
                    //SubTitle: 'Work, you bastard!',
                    LogoImg: result,
                  };

                  this.setState({ dataSource: this.state.dataSource.cloneWithRows(newDs) });
                  break;
                }
              }
            }
          }
          catch (e)
          {
            console.log("*** Could not load logo [" + docDef.Logo + "] : " + e);
          }
        });
      }
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  /**
  * Format the docDefinition data into a listview ready data source. This means we're sorting the data alphabetically,
  * and prettify dates as required
  **/
  formatData(data)
  {
    const resultData = [];
    let rowId = 0;

    // We're sorting by alphabetically and numerically so we need the alphabet. There is probably an easier sorting
    // method available, but I've done it this way partially so that I can later on introduce sections easily if needed
    const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    for (let idx = 0; idx < alphabet.length; idx++)
    {
      // Get docs whose ShortName starts with the current letter
      const docs = data.filter((doc) => doc.ShortName.toUpperCase().indexOf(alphabet[idx]) === 0);
      if (docs.length > 0)
      {
        //console.log("*** We have a match for letter : " + alphabet[idx]);
        // Loop over all matching docTypes
        for (let i = 0; i < docs.length; i++)
        {
          // Make the dates a bit more presentable. Also make sure we can re-run it without getting an illegal date format
          if ((docs[i].ValidFrom.length <= 0) || (docs[i].ValidFrom === 'Undefined'))
            docs[i].ValidFrom = 'Undefined';
          else
            docs[i].ValidFrom = Moment(docs[i].ValidFrom).format('DD MMM YYYY');  // use default formatting

          if ((docs[i].ValidTo.length <= 0) || (docs[i].ValidTo === 'Open Ended'))
            docs[i].ValidTo = 'Open Ended';
          else
            docs[i].ValidTo = Moment(docs[i].ValidTo).format('DD MMM YYYY');  // use default formatting

          docs[i].LogoImg = 'data:image/gif;';   // initialise this so we always have a LogoImg field we can check

          // Store the data we care about for this row
          resultData.push(docs[i]);
        }
      }
    }

    return resultData;
  }

  /**
  * Format the docDefinition data into a listview ready data source. This means we're sorting the data alphabetically,
  * create sections and push the data into a dataBlob, indexed by a rowId
  * Not used at the moment, but leaving this in if we need to start using sections.
  **/
  formatDataOld(data)
  {
    console.log("*** # formatData");
    // We're sorting by alphabetically and numerically so we need the alphabet
    const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

    // Need somewhere to store our data
    const dataBlob = {};
    const sectionIds = [];
    const rowIds = [];

    // Each section is going to represent a letter in the alphabet so we loop over the alphabet
    for (let sectionId = 0; sectionId < alphabet.length; sectionId++)
    {
      // Get the character we're currently looking for
      const currentChar = alphabet[sectionId];

      // Get docs whose ShortName starts with the current letter
      const docs = data.filter((doc) => doc.ShortName.toUpperCase().indexOf(currentChar) === 0);

      // If there are any docs who have a shortname starting with the current letter then we'll
      // add a new section otherwise we just skip over it
      if (docs.length > 0)
      {
        // Add a section id to our array so the listview knows that we've got a new section
        sectionIds.push(sectionId);

        // Store any data we would want to display in the section header. In our case we want to show
        // the current character
        dataBlob[sectionId] = { character: currentChar };

        // Setup a new array that we can store the row ids for this section
        rowIds.push([]);

        // Loop over the valid users for this section
        for (let i = 0; i < docs.length; i++)
        {
          // Create a unique row id for the data blob that the listview can use for reference
          const rowId = `${sectionId}:${i}`;

          console.log("*** # adding rowId : " + rowId + " for currentChar [" + currentChar + "]");

          // Push the row id to the row ids array. This is what listview will reference to pull
          // data from our data blob
          rowIds[rowIds.length - 1].push(rowId);

          // Make the dates a bit more presentable. Also make sure we can re-run it without getting an illegal date format
          if ((docs[i].ValidFrom.length <= 0) || (docs[i].ValidFrom === 'Undefined'))
            docs[i].ValidFrom = 'Undefined';
          else
            docs[i].ValidFrom = Moment(docs[i].ValidFrom).format('DD MMM YYYY');  // use default formatting


          if ((docs[i].ValidTo.length <= 0) || (docs[i].ValidTo === 'Open Ended'))
            docs[i].ValidTo = 'Open Ended';
          else
            docs[i].ValidTo = Moment(docs[i].ValidTo).format('DD MMM YYYY');  // use default formatting

          docs[i].LogoImg = 'data:image/gif;';   // initialise this so we always have a LogoImg field we can check

          // Store the data we care about for this row
          dataBlob[rowId] = docs[i];
        }
      }
    }

    return { dataBlob, sectionIds, rowIds };
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    //console.log('*** Render LogoImg : ' + this.state.rawData.dataBlob[this.state.rawData.rowIds[2]]);

    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    const { navigate, goBack } = this.props.navigation;

    return (
      <SafeAreaView style={styles.safeArea}>

        <Text style={[styles.dataTextTitle, {color: colors.fgTextLight}]}>
          Available Documents
        </Text>

        <ListView
          style={styles.docsListView}
          dataSource={this.state.dataSource}
          renderRow={(rowData) => <Row {...rowData} />}
          renderSeparator={(rowId) => <View key={rowId} style={styles.separator1} />}
        />

        <View style={{backgroundColor: colors.headerColor}}>
          <TouchableHighlight
            style={styles.submitSmall}
            onPress={() => goBack(null)}
            underlayColor='#fff'>
            <Text style={styles.submitText}>Home</Text>
          </TouchableHighlight>
        </View>
      </SafeAreaView>
    );
  }

  // --------------------------------------------------------------------------------------------------------------------

}

DocScreen.navigationOptions = {
  header: null
};

export default DocScreen;
