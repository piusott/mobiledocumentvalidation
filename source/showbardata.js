'use strict';
import React, { Component } from 'react';
import ReactNative, {
  AppRegistry,
  Dimensions,
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
} from 'react-native';

// --------------------------------------------------------------------------------------------------------------------

export default class BarcodeValidation extends Component {


  // TODO : copy the decipherbarcode functions from main and format the output properly. E.g. make sure the dates
  //        are shown properly, the backdrop is shown as red or green depending on whether the barcode is valid, the
  //        document title is translated, etc.
  //        I want to be able to call this function from the render function of the main class
  render() {
    console.log('BarcodeValidation render function called');
    return (
      <View style={styles.container}>

        <Text style={styles.welcome}>Barcode Scanner</Text>

        <View style={styles.containerPass}>

        <Text style={styles.label}>Hello world mid!</Text>

        <View>
          <Text style={styles.label}>Document Type: </Text>
          <Text style={styles.dataText}>{this.docType}</Text>
        </View>
        <View>
          <Text style={styles.label}>Holder Name: </Text>
          <Text style={styles.dataText}>{this.holderName}</Text>
        </View>
          <View style={{flex:0, flexDirection: 'row'}}>
            <Text style={styles.label}>Holder ID: </Text>
            <Text style={styles.dataText}>{this.holderId}</Text>
          </View>
          <View style={{flex:0, flexDirection: 'row'}}>
            <Text style={styles.label}>Date of Birth: </Text>
            <Text style={styles.dataText}>{this.dob}</Text>
          </View>
          <View style={{flex:0, flexDirection: 'row'}}>
            <Text style={styles.label}>Issue Date: </Text>
            <Text style={styles.dataText}>{this.issueDate}</Text>
          </View>
          <View style={{flex:0, flexDirection: 'row'}}>
            <Text style={styles.label}>Time Stamp: </Text>
            <Text style={styles.dataText}>{this.timeStamp}</Text>
          </View>
        </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  containerPass: {
    flex: 1,
    width: 250,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
  },
  containerFail: {
    flex: 1,
    width: 250,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  capture: {
    flex: 0,
    backgroundColor: '#C0C0C0',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  },
  dataText: {
    flex: 0,
    textAlign: 'left',
    color: '#3333FF',
    marginBottom: 2,
  },
  label: {
    flex: 0,
    textAlign: 'left',
    color: '#333333',
    marginBottom: 2,
  }
});
