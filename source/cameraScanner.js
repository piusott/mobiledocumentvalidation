/**
 * ABCorp Barcode Validation app
 * Camera barcode scanner class
 * Pius Ott - 20170811
 * @flow
 */

 'use strict'
 import React, { Component } from 'react';
 import {
   StyleSheet,
   Text,
   TouchableHighlight,
   Button,
   View,
 } from 'react-native';
//import QRCodeScanner from 'react-native-qrcode-scanner';
import QRCodeScanner from './barcodeScanner';

const styles = require('./style.js').style;
const colors = require('./style.js').color;

// --------------------------------------------------------------------------------------------------------------------

class CameraScanner extends Component {


  constructor(props) {
    super(props);

    this.scanner = null;
  }

  // ------------------------------------------------------------------------------------------------------------------

  /*
  * When running in the emulator we can't use the camera. This function allows us to simulate what a barcode read would
  * result in. There a couple of valid as well as fake barcodes here.
  */
  simulateBarcodeRead() {
    // test array of a couple of valid and invalid barcodes, only used for this demo
    const sampleBarcodes = [
      'VT432|25189207|TT6356|WCK-123|Pius Ott|813/422 Collins St.|Melbourne, Vic, 3000||1996|Ford|Laser KJ|Sedan|334512|20170728|MEUCIEIN3bHAn9Z2KZQVN0IfRK+FAzbT9VgIgw+1cm918ExQAiEA0Rbij7b2hEnEHw/BYGE3c04t3ZQsoQcctn9gQ0+rk3Y=',
      'B1234|25192092|109-2006-150001|Pius Thomas Ott|19650914|Schwyz, Switzerland|Anton Constantin Ott|Marta Josefa Scheuber|20170728|Male|MEUCIQDJbSL7YXdGq1pnSu4/c7S5f5a9gCZNcifm1bt3K0rLTAIgLtDQvlRybzX0X5vedmHMJTTeSE1UnenWByPIp7bYMKw=',
      'B1234|25192092|176-2012-140332|John Doe Fake|19870318|Melbourne, Australia|Brian James Fake|Jan Forge|20170728|Male|MEUCIQDJbSL7YXdGq1pnSu4/c7S5f5a9gCZNcifm1bt3K0rLTAIgLtDQvlRybzX0X5vedmHMJTTeSE1UnenWByPIp7bYMKw=',
      'M0001|25192216|19577664939|770631|0014|984.18|20171124|Walter Smith|1175 Washington Blvd.|Stamford, CT, 44325|Lydia Jones|632 Nicholson St.|Green Bay, MI, 76432|Testing money orders|MEUCIFu+Z+7X4CyjqpU76obPDZOVAzkGPnsQ6rcyf9OmppYzAiEA7hXzyJFSNc/fIAlGN8wD3BTgO5eLuw3yYmGUBT9xfNs=',
      'M0001|25192216|41592346698|703212|0120|1200.00|20171021|Walter Fake|1175 Washington Blvd.|Stamford, CT, 44325|Lydia Forger|115 Brunswick St.|Stanton, GA, 25022|Fake money orders|MEUCIFu+Z+7X4CyjqpU76obPDZOVAzkGPnsQ6rcyf9OmppYzAiEA7hXzyJFSNc/fIAlGN8wD3BTgO5eLuw3yYmGUBT9xfNs=',
//      'U9876|25192339|2900414|Pius Thomas Ott|19650914|Monash Caulfield|Bachelor of Computing|Software Development|3.040|20170728|MEUCIGN+AowdK3BoqgOtGIWeRj3YrGuKWnKCDfJ+1MvhMWwvAiEA95B5BtsKmi6tRvQA06meGPGh2M+bwuVigD8VQqjJwCo=',
      //'VT431|PV|25043704|20170728|John Doe Fake|19750704|NLK-1890|1:45724;2:Ford Laser KJ 2012;3:SN25828374;4:999 Any Lane,Any City,AS,99999|MEYCIQCf2XHdJ00MtFagkwTYvfRvip58ZsZxrxgG1FbuFayR3AIhANDV4LtoY+IK5DUe8Wm53i/2CwDVf+A5qacrqtPlfkEM',
      //'VT431|PV|25043713|20170728|Jane Doe Real|19770811|CIQ-5233|1:12877;2:Toyota LX 2014;3:SN6345-2345;4:999 Any Lane,Any City,AS,99999|MEQCIFwnm2QFDsj899/Oyo5zFJv/2Wwe/28cICEdW3rFeiv1AiA9F9rqU0zUR2cKheFZ+106B0qHi1MT7SbkQ98PhKYrYA==',
      'DLCT1|25399286|S764325789|10093 002|Christina|L|CARPENTER|19880314|F|5-06|120|110 Maple Street|Anytown, MN 12345|D|None|None|20240630|20141118|MEUCIHLgN4/A9KC9zrZFMUsHSDosWZb94zPDiIBWUcBHbVyRAiEA7nIE4rYgRHk62+FcbnVqjle4er1KWRq1J9uceoTd11k=',
      'UT001|25383800|Graduate School|Engineering Mgmt.|James|TRANSCRIPT|123 Anystreet|Anycity, AS, 99999|19770707|36|36|114|3454|UTO_ENGM_6795342|EM263988|MEUCIQD8WP+CMPPyGTXBffnIFP9MF7gCyHOZVSges2Nk60coLAIgeQeDAIuex978l/sRL8R/PKk/+BfNMQSiWwE5yhIIZTg=',
      'VS001|25383805|Business|Susan Ann|STEVENSON|L56224533|19710722|456789|20180111|20190110|MULTIPLE|MEUCIQD8WP+CMPPyGTXBffnIFP9MF7gCyHOZVSges2Nk60coLAIgeQeDAIuex978l/sRL8R/PKk/+BfNMQSiWwE5yhIIZTg=',
      'ID001|25392659|National ID|S000123456789|0130002456789|Christina Louise|CARPENTER|110 Maple Street|Anytown, MN 12345|19810314|20151118|20191117|BROWN|5-06|MEYCIQCOHIYqxAztszPJ/RldMyRrMR/XKqRGaBEUDABl6VRirAIhAImNQC3YBbxPYrmhP3j0v33w3+pjB7qtoalfpx/1DbOT',
    ];

    console.log('*** cameraScanner simulator called. We have ' + sampleBarcodes.length + ' sample barcodes');

    var randomIndex = Math.floor(Math.random() * sampleBarcodes.length);
    //var randomIndex = 3;
    return sampleBarcodes[randomIndex];
  }

  // ------------------------------------------------------------------------------------------------------------------

  _onBarCodeRead = (data) => {
    //var jsonData = JSON.stringify(data);
    console.log('*** Barcode received : ' + JSON.stringify(data));

    if (data !== null)
    {
      this.barcodeData = data.data;
      this.props.barcodeReceivedHandler(this.barcodeData);
    }

    //console.log('*** Bardata : ' + this.barcodeData);
/*
*** Barcode received : {"bounds":[{"y":"832.0","x":"272.0"},{"y":"387.72223","x":"254.0"},{"y":"404.0","x":"718.0"},{"y":"847.0","x":"670.75"}],"type":"QR_CODE","data":"M0001|25393670|19577664939|770631|0014|984.18|20171124|Walter Smith|1175 Washington Blvd.|Stamford, CT, 44325|Lydia Jones|632 Nicholson St.|Green Bay, MI, 76432|Testing money orders|MEUCIQD2j4eZA9j1atYLH62oHG4n3IyWjBkqoFmh0bq+2aeZkgIgEtvjJRkot05mpu6uXtYtKgr3QIhUDH5UoMNHpgHEnL0=","target":55}
05-02
*/
    // Allow further barcode scans, in case what we read was an unrecognised barcode

    try
    {
      setTimeout(() => {
        if (this.scanner !== null)
        {
          this.scanner.reactivate();
        }
      }, 1000);
    }
    catch (ex)
    {
      console.log('*** Exception on resetting barcode scanner : ' + ex);
    }
  };

  // ------------------------------------------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    // show the simulate button only if we're in debugging mode
    var simBtn;
    if (configSettings.allowSimulation)
    {
      simBtn = <Text style={localStyles.blueButton}
                  onPress={() => this.props.barcodeReceivedHandler(this.simulateBarcodeRead())}>
                  Testing - Simulate Read
                </Text>
    }

    var statusText = 'Center Barcode In Window';
    var statusColour = colors.fgTextLightGray;
    if (this.props.isABCorpBarcode === false)
    {
      statusColour = colors.fgTextError;
      statusText = 'Not a valid ABCorp barcode';
    }

    return (
      <View style={localStyles.rectangleContainer}>

        <QRCodeScanner
          onRead={this._onBarCodeRead}
          topContent={<Text style={[styles.validStateLabelFail, {color:statusColour}]}>{statusText}</Text>}
          ref={(node) => { this.scanner = node }}
          fadeIn={false}
          cameraType={'back'}
          showMarker={true}
        />

        {simBtn}
      </View>
    )
  }
}

// --------------------------------------------------------------------------------------------------------------------

const localStyles = StyleSheet.create({

  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  blueButton: {
    backgroundColor: '#1A7FB6',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 10,
    marginTop: 10,
    color: '#FFFFFF',
    fontWeight: 'bold',
  }
});

export default CameraScanner;
