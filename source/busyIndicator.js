/**
 * Busy indicator that we can show while running barcode validation.
 * This is covers the whole screen but is also transparent so that we can essentially "disable" the screen behind
 * it and only show the activity indicator in the screen centre.
 * Pius Ott - 20180423
 * @flow
 */

'use strict'
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  ActivityIndicator
} from 'react-native';

const BusyIndicator = (props) => {
  const {
    validating,
    displayText,
    ...attributes
  } = props;

const styles = require('./style.js').style;
//const colors = require('./style.js').color;

return (
    <Modal
      transparent={true}
      animationType={'none'}
      onRequestClose={() => {}}
      visible={validating}>
      <View style={localStyles.modalBackground}>
        <View style={localStyles.activityIndicatorWrapper}>
          <Text style={[localStyles.titleText, {marginLeft:'5%', marginRight:'5%'}]}>{displayText}</Text>
          <ActivityIndicator animating={validating} />
        </View>
      </View>
    </Modal>
  )
}

const localStyles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 150,
    width: 300,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  titleText:{
    //color: colors.fgTextDark,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '800',
  },
});

export default BusyIndicator;
