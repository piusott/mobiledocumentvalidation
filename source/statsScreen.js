import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  FlatList,
  TouchableHighlight,
} from "react-native";
import {
  List,
  ListItem,
} from "react-native-elements"
import Moment from 'moment';

const styles = require('./style.js').style;
const colors = require('./style.js').color;

const maxBarDataLength = 40;  // display length of failed validation barcode data

class StatsScreen extends Component
{

  // ------------------------------------------------------------------------------------------------------------------

  renderLogSumItem({item})
  {
    let formattedDate = Moment(item.lastScan).format('D MMMM YYYY - h:mm:ss a');

    let headerColour = colors.fgTextLight;

    let subTitle = `Successful Scans : ${item.totalPass}\nValidation Failed : ${item.totalFail}\nLast Scan : ${formattedDate}`;

    return (
      <ListItem
        inverted
        hideChevron
        subtitleNumberOfLines={3}
        titleStyle={{color: colors.fgTextLight, marginTop:0}}
        title={`${item.key}`}
        subtitle={subTitle}
     />
    )
  }

  // ------------------------------------------------------------------------------------------------------------------

  renderLogFailItem({item})
  {
    //{key: now, docType : docType, scannedData: rawData, location: ''}

    let formattedDate = Moment(item.key).format('D MMMM YYYY - h:mm:ss a');

    var barData = '';
    var idxData = item.scannedData.indexOf('|');
    if (idxData > 0)
    {
      // show some display data without repeating the first field, docType.
      idxData++;
      if (item.scannedData.length > (maxBarDataLength - idxData))
      {
        barData = item.scannedData.substring(idxData, maxBarDataLength) + '...';
      }
      else
      {
        barData = item.scannedData.substring(idxData);
      }
    }

    var formattedSubtitle = `${item.docType}\n${barData}`;

    return (
      <ListItem
        inverted
        hideChevron
        subtitleNumberOfLines={2}
        titleStyle={{color: colors.failTitleColor, marginTop:0}}
        title={formattedDate}
        subtitle={formattedSubtitle}
     />
    )
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const { navigate, goBack } = this.props.navigation;

    return (
      <SafeAreaView style={styles.safeArea}>
        <Text style={[styles.dataTextTitle, {backgroundColor: colors.white, color: colors.fgTextDark, marginTop:0}]}>
          Statistics
        </Text>

        <FlatList
          style={{backgroundColor: colors.bgEmphasis}}
          data={scanLogSumData}
          renderItem={this.renderLogSumItem}
        />

        <Text style={[styles.dataTextTitle, {backgroundColor: colors.white, color: colors.fgTextDark, marginTop:5}]}>
          Failed Validations
        </Text>

        <FlatList
          style={{backgroundColor: colors.bgEmphasis}}
          data={scanLogFailData}
          renderItem={this.renderLogFailItem}
        />

        <View style={{backgroundColor: colors.headerColor}}>
          <TouchableHighlight
            style={styles.submitSmall}
            onPress={() => goBack(null)}
            underlayColor='#fff'>
            <Text style={styles.submitText}>Home</Text>
          </TouchableHighlight>
        </View>

      </SafeAreaView>
    );
  }

}

// --------------------------------------------------------------------------------------------------------------------

StatsScreen.navigationOptions = {
  title: "Statistics",
  header: null
};

export default StatsScreen;
