/**
 * ABCorp Barcode Validation app
 * Scan logging class
 * We maintain two logs. A basic summary that holds the number of pass/fails for each document type and then a second
 * log that holds further details of all failed validations.
 * As a future addition, we should pass on the failed log to ABCorp
 * Pius Ott - 20180207
 */

'use strict'
import React, { Component } from 'react';
import {
   AsyncStorage,
} from 'react-native';
import Moment from 'moment';

// --------------------------------------------------------------------------------------------------------------------

// Storage locations of our log files
const storageKeySum = '@BarcodeVal:scanLog';
const storageKeyDetail = '@BarcodeVal:scanFailLog';

global.scanLogSumData = [];
global.scanLogFailData = [];

export default class ScanSynchLog extends Component {

  constructor(props)
  {
    super(props);

    this.LoadScanLogs();    // load saved logs
  }

  // ------------------------------------------------------------------------------------------------------------------

  static getScanLogIndex(docType)
  {
    function FindShortName(element)
    {
      return element.key === docType;
    }

    return scanLogSumData.findIndex(FindShortName);
  }

  // ------------------------------------------------------------------------------------------------------------------

  static UpdateScanLog(docType, isPass, rawData)
  {
    var now = Moment();
    console.log('*** UpdateScanLog : docType [' + docType + '] - result [' + isPass + ']');

    var idx = this.getScanLogIndex(docType);
    console.log('*** scanLogSumData index for docType [' + docType + '] = [' + idx + ']');

    var addPass = 0;
    var addFail = 0;
    if (isPass)
    {
      addPass = 1;
    }
    else
    {
      addFail = 1;
    }

    if (idx < 0)
    {
      // First scan for this docType. Add new item to array
      var tmpArray = scanLogSumData.slice();
      tmpArray.push({key: docType, totalPass: addPass, totalFail: addFail, lastScan: now});
      scanLogSumData = tmpArray;
    }
    else
    {
      scanLogSumData[idx].key = docType;
      scanLogSumData[idx].totalPass = scanLogSumData[idx].totalPass + addPass;
      scanLogSumData[idx].totalFail = scanLogSumData[idx].totalFail + addFail;
      scanLogSumData[idx].lastScan = now;
    }

    // If validation has failed, record some extra details.
    if (isPass === false)
    {
      // TODO
      // For the time being, we're just appending the fail log. I don't expect this to grow huge, but nevertheless
      // we need to eventually make this sustainable by archiving or dumping old log data.

      // TODO
      // I'm not recording the location yet. This is probably not needed, but I also don't want to have to get the
      // user to approve having the app access location data.
      var tmpArray = scanLogFailData.slice();
      tmpArray.push({key: now, docType : docType, scannedData: rawData, location: ''});
      scanLogFailData = tmpArray;
    }

    this.SaveScanLogs(isPass === false);
  }

  // ------------------------------------------------------------------------------------------------------------------

  static SaveScanLogs(doFailUpdate)
  {
    try
    {
      console.log("*** Saving scanLogSumData");
      AsyncStorage.setItem(storageKeySum, JSON.stringify(scanLogSumData));
      if (doFailUpdate)
      {
        console.log("*** Also saving scanLogFailData");
        AsyncStorage.setItem(storageKeyDetail, JSON.stringify(scanLogFailData));
      }
    }
    catch (e)
    {
      console.log("*** Exception in SaveScanLogs : " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  LoadScanLogs()
  {
    try
    {
      AsyncStorage.getItem(storageKeySum, (err, result) =>
      {
        let scanLog = JSON.parse(result)
        console.log("*** loaded scanLogSumData JSON : " + result);
        if (scanLog != null)
        {
          scanLogSumData = scanLog;
        }
        else
        {
          scanLogSumData = [];  // initialise a blank log
        }
      });
    }
    catch (e)
    {
      console.log("*** Could not load scanLogSumData from local : " + e);
    }

    try
    {
      AsyncStorage.getItem(storageKeyDetail, (err, result) =>
      {
        let scanLog = JSON.parse(result)
        console.log("*** loaded scanLogFailData JSON : " + result);
        if (scanLog != null)
        {
          scanLogFailData = scanLog;
        }
        else
        {
          scanLogFailData = [];  // initialise a blank log
        }
      });
    }
    catch (e)
    {
      console.log("*** Could not load scanLogFailData from local : " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

}
