/**
 * ABCorp Barcode Validation app
 * Global settings class
 * Pius Ott - 20171222
 */

 'use strict'
 import React, { Component } from 'react';
 import {
   AsyncStorage,
 } from 'react-native';
import DeviceInfo from 'react-native-device-info';

// --------------------------------------------------------------------------------------------------------------------

//const defaultServerURL = 'http://172.31.64.64/barcodeval/';// TODO : move default to a public server once we have one
var defaultServerURL = 'http://abidtest.abnote.com.au/barcodevaltest/'
const storageKey = '@BarcodeVal:config';    // Location of our config settings

global.configSettings = null;

export default class ConfigSettings extends Component {

  constructor(props)
  {
    super(props);

    if (DeviceInfo.isEmulator())
    {
      // If we're running in the simulator, ABCorp firewall restrictions don't allow us easy access to the perso server,
      // so use a local web server address instead. Note that these are different servers, so we need to make sure
      // the perso details are synched.
      console.log('*** Running in simulator');
      defaultServerURL = 'http://172.31.64.64/barcodeval/';
    }

  }

  // ------------------------------------------------------------------------------------------------------------------

  static resetConfig()
  {
    configSettings.allowSimulation = false;
    configSettings.doPDF417 = true;
    configSettings.doQR = true;
    configSettings.serverURL = defaultServerURL;
  }

  // ------------------------------------------------------------------------------------------------------------------

  static saveConfig()
  {
    try
    {
      AsyncStorage.setItem(storageKey, JSON.stringify(configSettings));
    }
    catch (e)
    {
      console.log("*** Error loading configSettings : " + e);
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  /**
  * Load the config settings definitions. If no settings are found, create default settings and store them.
  */
  loadConfigSettings()
  {
    return new Promise(function(resolve, reject)
    {
      // try and load the settings from our local storage
      try
      {
        AsyncStorage.getItem(storageKey, (err, result) =>
        {
          let docDef = JSON.parse(result)
          console.log("*** load Config from AsyncStorage result : " + result);

          if ((result === null) || (result === undefined))
          {
            console.log("*** No config found. Create a new one and store it");
            configSettings = {
              doPDF417: true,
              doQR: true,
              serverURL: defaultServerURL,
              allowSimulation: false,
            };

            AsyncStorage.setItem(storageKey, JSON.stringify(configSettings));
          }
          else
          {
            console.log("*** Config found. Parse it");
            configSettings = JSON.parse(result);
            //console.log("*** Config created. configSettings.serverURL = [" + configSettings.serverURL + "]");
          }

          console.log("*** Config loaded, resolve promise.");
          resolve();
        });
      }
      catch (e)
      {
        console.log("*** Error loading configSettings : " + e);
        reject(e);
      }
    });
  }

  // ------------------------------------------------------------------------------------------------------------------
}
