/**
 * ABCorp Barcode Validation app
 * Pius Ott - 20170721
 * @flow
 */

'use strict'
import React, { Component } from 'react';
import {
  Button,
  Keyboard,
  TouchableHighlight,
  Text,
  TextInput,
  View,
  Image,
  ImageBackground,
  ListView,
  ScrollView,
  Switch,
  SafeAreaView,
} from 'react-native';


const styles = require('./style.js').style;
const colors = require('./style.js').color;

import ConfigSettings from './configSettings';

class SetupScreen extends Component {

  constructor(props)
  {
    super(props);

    this.state = { currentURL: configSettings.serverURL };

    console.log("*** setupScreen, using configSettings.serverURL = [" + configSettings.serverURL + "]");
  }

  // testing - The keyboard sometimes seems a bit 'sticky', so I want to log exactly when it shows and hides
  componentWillMount()
  {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount()
  {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow()
  {
    console.log("*** Keyboard did show");
  }

  _keyboardDidHide()
  {
    console.log("*** Keyboard hidden");
  }

  // ------------------------------------------------------------------------------------------------------------------

  togglePDF417Switch = (value) =>
  {
    Keyboard.dismiss();

    configSettings.doPDF417 = value;

    if ((configSettings.doQR === false) && (configSettings.doPDF417 === false))
      configSettings.doQR = true;   // make sure at least either of the two is active

    ConfigSettings.saveConfig();
    this.forceUpdate();

    console.log("*** doPDF417 Changed : " + value);
  }

  toggleQRSwitch = (value) =>
  {
    Keyboard.dismiss();

    configSettings.doQR = value;
    if ((configSettings.doQR === false) && (configSettings.doPDF417 === false))
      configSettings.doPDF417 = true;   // make sure at least either of the two is active

    ConfigSettings.saveConfig();
    this.forceUpdate();
    console.log("*** doQR Changed : " + value);
  }

  toggleSimSwitch = (value) =>
  {
    Keyboard.dismiss();

    configSettings.allowSimulation = value;
    ConfigSettings.saveConfig();
    this.forceUpdate();
    console.log("*** allowSimulation Changed : " + value);
  }

  updateServerURL()
  {
    Keyboard.dismiss();

    console.log("*** serverURL changed : " + this.state.currentURL);
    configSettings.serverURL = this.state.currentURL;
    ConfigSettings.saveConfig();
    //this.forceUpdate();
  }

  resetAll()
  {
    console.log("*** resetting all configuration");

    ConfigSettings.resetConfig();
    ConfigSettings.saveConfig();

    this.forceUpdate();
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const { navigate, goBack } = this.props.navigation;

    return (
      <SafeAreaView style={styles.safeArea}>

        <Text style={[styles.dataTextTitle, {color: colors.fgTextLight}]}>Settings</Text>

        <ScrollView style={{margin:'1%', backgroundColor:colors.bgEmphasis}}>
          <View style={{flexDirection: 'column'}}>
            <Text style={[styles.infoTextTitle, {fontSize:16, paddingBottom:2, paddingLeft: 4, color: colors.fgTextMedGray}]}>BARCODE TYPES</Text>
            <View style={styles.separator2} />

            <View style={[styles.container2, { }]}>
              <View style={styles.containerConfigIcon}>
                <Image style={styles.iconImg} source={require('./images/Icons/BarCode1.png')} />
              </View>
              <View style={styles.containerConfigItem} >
                <Text style={[styles.infoText2, {marginTop:5}]}>PDF417</Text>
                <Switch
                  onValueChange={ (value) => this.togglePDF417Switch(value)}
                  value = {configSettings.doPDF417}
                  onTintColor = {colors.bgGreen}
                  thumbTintColor = {colors.fgTextLightGray}
                />
              </View>
            </View>

            <View style={[styles.separator2, {marginLeft:10, marginRight:10}]} />

            <View style={styles.container2}>
              <View style={styles.containerConfigIcon}>
                <Image style={styles.iconImg} source={require('./images/Icons/BarCode1.png')} />
              </View>
              <View style={styles.containerConfigItem}>
                <Text style={[styles.infoText2, {marginTop:5}]}>QR Code</Text>
                <Switch
                  onValueChange={ (value) => this.toggleQRSwitch(value)}
                  value = {configSettings.doQR}
                  onTintColor = {colors.bgGreen}
                  thumbTintColor = {colors.fgTextLightGray}
                />
              </View>
            </View>
          </View>

          <View style={styles.separator2} />

          <View style={{height: 30}}>
          </View>

          <View style={{flexDirection: 'column'}}>
            <Text style={[styles.infoTextTitle, {fontSize:16, paddingBottom:2, paddingLeft: 4, color: colors.fgTextMedGray}]}>SYNCHRONISATION SERVER</Text>
            <View style={styles.separator2} />

            <View style={styles.container2}>
                <View style={styles.containerConfigIcon}>
                  <Image style={styles.iconImg} source={require('./images/Icons/ServerURL1.png')} />
                </View>
                <View style={[styles.containerConfigItem, {}]}>
                  <Text style={[styles.infoText2, {marginTop:5}]}>Server URL</Text>
                </View>
            </View>
            <TextInput
              style={styles.textBox}
              placeholder={configSettings.serverURL}
              onChangeText={(currentURL) => this.setState({currentURL})}
              onSubmitEditing={() => this.updateServerURL()}
            />
            {/*onSubmitEditing={(text) => this.setState({text})}*/}
          </View>

          <View style={styles.separator2} />

          <View style={{height: 30}}>
          </View>

          <View style={{flexDirection: 'column'}}>
            <Text style={[styles.infoTextTitle, {fontSize:16, paddingBottom:2, paddingLeft: 4, color: colors.fgTextMedGray}]}>TESTING</Text>
            <View style={styles.separator2} />

            <View style={styles.container2}>
              <View style={styles.containerConfigIcon}>
                <Image style={styles.iconImg} source={require('./images/Icons/Testing1.png')} />
              </View>
              <View style={styles.containerConfigItem} >
                <Text style={[styles.infoText2, {marginTop:5}]}>Allow Simulation</Text>
                <Switch
                  onValueChange={ (value) => this.toggleSimSwitch(value)}
                  value = {configSettings.allowSimulation}
                  onTintColor = {colors.bgGreen}
                  thumbTintColor = {colors.fgTextLightGray}
                />
              </View>
            </View>

            <View style={[styles.separator2, {marginLeft:10, marginRight:10}]} />

            <View style={styles.container2}>
              <View style={styles.containerConfigIcon}>
                <Image style={styles.iconImg} source={require('./images/Icons/Reset1.png')} />
              </View>
              <View style={styles.containerConfigItem}>
                <Text style={[styles.infoText2, {marginTop:5}]}>Reset All Settings</Text>
                <Switch
                  onValueChange={ (value) => this.resetAll(value)}
                  value = {false}
                  onTintColor = {colors.bgGreen}
                  thumbTintColor = {colors.fgTextLightGray}
                />
              </View>
            </View>
          </View>

          <View style={styles.separator2} />

        </ScrollView>

        <View style={{backgroundColor: colors.headerColor}}>
          <TouchableHighlight
            style={styles.submitSmall}
            onPress={() => goBack(null)}
            underlayColor='#fff'>
            <Text style={styles.submitText}>Home</Text>
          </TouchableHighlight>
        </View>

      </SafeAreaView>
    );
  }

  // --------------------------------------------------------------------------------------------------------------------

}

SetupScreen.navigationOptions = {
  header: null
};

export default SetupScreen;
