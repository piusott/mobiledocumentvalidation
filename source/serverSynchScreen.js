/**
 * ABCorp Barcode Validation app
 * Pius Ott - 20170721
 * @flow
 */

import React, { Component } from "react";
import {
  Alert,
  Text,
  View,
  SafeAreaView,
  FlatList,
  TouchableHighlight,
 } from "react-native";
import {
  List,
  ListItem,
} from "react-native-elements"
import ServerSynch from './docDefSynch';
import Moment from 'moment';
import BusyIndicator from './busyIndicator';

const styles = require('./style.js').style;
const colors = require('./style.js').color;

class ServerSynchScreen extends Component
{

  constructor(props)
  {
    super(props);

    this.state = {
       refresh : false,  // just used to force a render of the flatlist
       isSynching : false,  // if true, show the busyIndicator
     };
  }

  // ------------------------------------------------------------------------------------------------------------------

  testServer()
  {
    let msg = 'Connection to ' + configSettings.serverURL;

    this.setState({
      isSynching: true,   // show the busyIndicator
    });

    //fetch('http://abidtest.abnote.com.au/barcodevaltest/', {
    fetch(configSettings.serverURL, {
      headers: {
        'Cache-Control': 'no-cache'
      }})
      .then(() => {
        this.setState({ isSynching: false }, () =>
        {
          // Run this of a delay to let the screen render and hide the busyIndicator before showing the alert
          this.delay(50).then( () => { Alert.alert(msg + ' successful'); } );
        })
      })
      .catch((error) => {
        this.setState({ isSynching: false }, () =>
        {
           console.log('*** ' + msg + ' failed - ' + error)
           this.delay(50).then( () => { Alert.alert(msg + ' failed - ' + error); } );
        })
      });
  }

  // ------------------------------------------------------------------------------------------------------------------

  delay(interval)
  {
      return new Promise(function(resolve)
      {
          setTimeout(resolve, interval);
      });
  }

  synchNow()
  {
    console.log('*** starting synchNow : ' + this.state.isSynching);

    try
    {
      //this.setState({ isSynching: true }, () => {
      //  console.log('*** callback 1  - updated isSynching : ' + this.state.isSynching);
      //});

      this.setState({ isSynching: true }, () =>
      {
        console.log('*** callback 2 - updated isSynching : ' + this.state.isSynching);
        let sync = new ServerSynch();
        sync.loadServerDocDef(false);

        // this is dumb, I know. But for some reason I can't get the promise resolve working within loadServerDocDef. I
        // suspect this is to do with a mixup with the fetch promise resolve in that function. Either way, just to
        // let me get the screen design done for the moment, I'm just doing a delayed refresh as an ugly patch.
        this.delay(5000).then( () => {
          this.setState({
             refresh: !this.state.refresh,
             isSynching: false,
          });
          console.log('*** synchNow - state set ');
        });
      });
    }
    catch (e)
    {
      console.log('*** Error on loading config settings : ' + e);
      Alert.alert('Documents Synchronisation', 'Synch failed : ' + e);
    }
    finally
    {
      if (this.state.isSynching === true)
      {
        console.log('*** resetting isSynching in finally ');
        this.setState({ isSynching: false, });
      }
    }
  }

  // ------------------------------------------------------------------------------------------------------------------

  renderItem({item})
  {
    let formattedDate = Moment(item.key).format('D MMMM YYYY - h:mm:ss a');
    //console.log('*** formattedDate [' + formattedDate + '] from [' + item.key + ']');
    //var now = Moment();
    //console.log('*** now formatted [' + now.format('DD MMM YYYY h:mm:ss') + '] from [' + now + ']');

    let formattedSubtitle = "dummy"
    let tasks = ""
    let headerColour = colors.fgTextLight;
    let numLines = 2;

    if (item.tasks.length > 0)
    {
      tasks = ` - ${item.tasks}`;
      numLines = 1;
    }

    if (item.status === "Success")
    {
      formattedSubtitle = `${item.status}${tasks}\n${item.url}`;
      numLines = 2;
    }
    else
    {
      formattedSubtitle = `${item.status}\n${item.error}\n${item.url}`;
      headerColour = colors.failTitleColor;
      numLines = 3;
    }

    return (
      <ListItem
        inverted
        hideChevron
        subtitleNumberOfLines={numLines}
        titleStyle={{color: headerColour, marginTop:0}}
        title={formattedDate}
        subtitle={formattedSubtitle}
     />
    )
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const { navigate, goBack } = this.props.navigation;

console.log('*** isSynching : ' + this.state.isSynching);
    return (
    <SafeAreaView style={styles.safeArea}>

      <BusyIndicator validating={this.state.isSynching}  displayText='Synching...' />

      <Text style={[styles.dataTextTitle, {color: colors.fgTextLight, marginTop:0}]}>
        Server Synchronisation
      </Text>

      <FlatList
        style={{backgroundColor: colors.bgEmphasis}}
        data={synchLogData}
        extraData={this.state.refresh}
        renderItem={this.renderItem}
      />

      <View style={{backgroundColor: colors.bgEmphasis, flexDirection: 'row'}}>
        <TouchableHighlight
          style={[styles.submitSmall2, {flex:1}]}
          onPress={() => this.synchNow()}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Synch Now</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[styles.submitSmall2, {flex:1}]}
          onPress={() => this.testServer(null)}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Test Server Connection</Text>
        </TouchableHighlight>
      </View>
      <View style={{backgroundColor: colors.bgEmphasis, flex:1}}>
      </View>

      <View style={{backgroundColor: colors.headerColor}}>
        <TouchableHighlight
          style={styles.submitSmall}
          onPress={() => goBack(null)}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Home</Text>
        </TouchableHighlight>
      </View>

    </SafeAreaView>
    );
  }

  // --------------------------------------------------------------------------------------------------------------------

}

ServerSynchScreen.navigationOptions = {
  title: "Server Synchronisation",
  header: null
};

export default ServerSynchScreen;
