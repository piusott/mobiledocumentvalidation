/**
 * ABCorp Barcode Validation app
 * Main Menu class. First screen shown.
 * Pius Ott - 20170721
 * @flow
 */

 'use strict'
 import React, { Component } from 'react';
 import {
   Alert,
   AppRegistry,
   Button,
   Dimensions,
   StyleSheet,
   View,
   Text,
   Image,
   ImageBackground,
   TouchableHighlight,
   SafeAreaView,
 } from 'react-native';
import {StackNavigator} from 'react-navigation';
import ServerSynch from './docDefSynch';
import ConfigSettings from './configSettings';
import ServerSynchLog from './serverSynchLog';
import ScanLog from './scanLog';
import BackgroundTimer from 'react-native-background-timer';

// --------------------------------------------------------------------------------------------------------------------

class MainMenu extends React.Component
//const MainMenu = props =>
{

  constructor(props)
  {
    super(props);
    console.log("*** MainMenu Constructor");

    this.docDefs = new ServerSynch();
    this.config = new ConfigSettings();

    // maintain server synch and scan logs in the background
    new ServerSynchLog();
    new ScanLog();

    // Create a background thread that attempts to synch our document definitions periodically. For the time being,
    // I'm setting this to run once per day. Since the synch is also performed at application startup, there's probably
    // no point in running this more often.
    const fullDayMS = 86400000; // milliseconds in one day
    //const fullDayMS = 6000;  // testing, run every six seconds
    const intervalId = BackgroundTimer.setInterval(() => {
        // this will be executed every 2000 ms
        // even when app is the the background
        console.log('*** Running interval docDefSynch from mainMenu');

        // since loadConfigSettings ran in componentDidMount, we assume that we have a server URL
        try
        {
          this.docDefs.loadServerDocDef();
        }
        catch (e)
        {
          console.log('*** Error on loading config settings : ' + error);
        }

    }, fullDayMS);

  }

  // ------------------------------------------------------------------------------------------------------------------

  componentDidMount()
  {
    // Load and synch document definitions
    console.log("*** MainMenu componentDidMount called");

    // loadServerDocDef uses serverURL initialised in loadConfigSettings, so use a promise here
    this.config.loadConfigSettings()
      .then( () => { this.docDefs.loadServerDocDef(true); })
      .catch(error => { console.log('*** Error on loading config settings : ' + error); });
  }

  // ------------------------------------------------------------------------------------------------------------------

  render()
  {
    const { navigate } = this.props.navigation;
    const styles = require('./style.js').style;
    const colors = require('./style.js').color;

    return (
      <SafeAreaView style={styles.safeArea}>
        <ImageBackground
          style={{flex:1, height: null, width: null}}
          source={require('./images/Backdrop.png')}
          resizeMode="contain" >

          <View style={{flex: 1}}>
            <View style={{height: '35%'}}>
            </View>

            <View style = {styles.menuRow}>
              <TouchableHighlight style={styles.buttonView}
                underlayColor={colors.bgColor}
                activeOpacity={0.85}
                onPress={() => navigate('ScanScreen')} >
                <Image style={styles.buttonImg} source={require('./images/Buttons/ScanButton.png')} />
              </TouchableHighlight>
              <TouchableHighlight style={styles.buttonView}
                underlayColor={colors.bgColor}
                activeOpacity={0.85}
                onPress={() => navigate('DocScreen')} >
                <Image style={styles.buttonImg} source={require('./images/Buttons/DocTypesButton.png')} />
              </TouchableHighlight>
            </View>

            <View style = {styles.menuRow}>
              <TouchableHighlight style={styles.buttonView}
                underlayColor={colors.bgColor}
                activeOpacity={0.85}
                onPress={() => navigate('ServerSynchScreen')} >
                <Image style={styles.buttonImg} source={require('./images/Buttons/SynchButton.png')} />
              </TouchableHighlight>
              <TouchableHighlight style={styles.buttonView}
                underlayColor={colors.bgColor}
                activeOpacity={0.85}
                onPress={() => navigate('SetupScreen')} >
                <Image style={styles.buttonImg} source={require('./images/Buttons/ConfigButton.png')} />
              </TouchableHighlight>
            </View>

            <View style = {styles.menuRow}>
              <TouchableHighlight style={styles.buttonView}
                underlayColor={colors.bgColor}
                activeOpacity={0.85}
                onPress={() => navigate('StatsScreen')} >
                <Image style={styles.buttonImg} source={require('./images/Buttons/StatsButton.png')} />
              </TouchableHighlight>
              <TouchableHighlight style={styles.buttonView}
                underlayColor={colors.bgColor}
                activeOpacity={0.85}
                onPress={() => navigate('AboutScreen')} >
                <Image style={styles.buttonImg} source={require('./images/Buttons/AboutButton.png')} />
              </TouchableHighlight>
              </View>


          </View>
        </ImageBackground>
      </SafeAreaView>
    );
    //                  onPress={() => this.props.barcodeReceivedHandler(this.simulateBarcodeRead())} />

  }

  // ------------------------------------------------------------------------------------------------------------------
}

// --------------------------------------------------------------------------------------------------------------------
MainMenu.navigationOptions = {
  header: null
};



export default MainMenu;
