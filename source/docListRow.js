import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const styles = require('./style.js').style;
const colors = require('./style.js').color;

const Row = (props) => (
  <View style={styles.containerLV}>

    <View style={{flex: 1}}>
      <View style={{backgroundColor: colors.bgEmphasis, justifyContent: 'center', alignItems: 'center'}}>
        <Image style={styles.docLogo}
          source={{uri: `${props.LogoImg}`}} />
      </View>

      <Text style={[styles.infoTextTitle, {fontWeight : 'bold'}]}>
        {`${props.ShortName}`}
      </Text>
      <Text style={styles.infoText}>
        {`${props.LongName}\n`}
        {`${props.SubTitle}`}
      </Text>

      <View style={{flexDirection: 'row'}}>
        <Text style={styles.dataText}>
          {`Valid From : \n`}
          {`Valid To : `}
        </Text>
        <Text style={styles.infoText}>
          {`${props.ValidFrom}\n`}
          {`${props.ValidTo}`}
        </Text>
      </View>
    </View>
  </View>
);

export default Row;
